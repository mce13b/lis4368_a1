> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368L: Advanced Web Application Development
## Marielle Estrugo

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and BitBucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (CHs 1-4)

#### README.md file should include the following items:

* Screenshots of running java Hello (#1 above);
* Screenshots of running http://localhost:9999 (#2 above, Step #4(b) in tutorial; 
* git commands with short descriptions
* BitBucket repo links: a) this assignment and b) the completed tutorial above (bitbucketstationslocations).

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - create new repository
2. git status - view what you have in the working tree
3. git add - add files to tree
4. git commit - files/changes added to repository (usually with comment of change)
5. git push - update remote repository files 
6. git pull - pull changes from remote repository
7. git rm - change name of file in repository

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot]![run_java.png](https://bitbucket.org/repo/8dqzE5/images/3302546658-run_java.png)


*Screenshot of running http://localhost:9999*:

![Tomcat Installation Screenshot]![tomcat.png](https://bitbucket.org/repo/8dqzE5/images/3760785409-tomcat.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mce13b/bitbucketstationslocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/mce13b/myteamquotes/ "My Team Quotes Tutorial")

##### Part 3:
*Chapters 1-4 Questions*

1.  A JavaBean, or bean, is a Java class that - *declares public instance variables*

2.  A servlet - *runs in a JSP*

3.  A set of pages that are generated in response to user requests is called a/an - *JSP*

4.  A web application is a type of - *client/server application*

5. After you edit a web.xml file, you can check to see if it is still valid by checking it against its - *XML schema*

6. An HTML form contains one or more controls like - *all of the above*

7. An HTTP request - *is sent from the web browser to a web server*

8. For web applications, data can be stored in - *all of the above*

9. In HTML, these do not display on their own lines in browsers. - *block elements*

10. Since the web.xml file describes how the web application should be configured when it is deployed on a server, the file is known as the - *deployment descriptor (DD)*

11. The HTML5 semantic elements include the - *all of the above*

12. The controller manages the flow of the application, and this work is done by one or more - *servlets*

13. The presentation layer for a typical servlet/JSP web application consists of - *HTML pages and JSPs*

14. The specification that describes how web servers can interact with all Java web technolgies is know as - *The Java Enterprise Edition (Java EE)*

15. To develop Java web applications, you can use an Integrated Development Environment (IDE) such as: - *NetBeans or Eclipse*

16. To transfer your web application files to a web server, you can use a/an ___________ client such as FileZilla. - *FTP (File Transfer Protocol)*

17. When you use the MVC pattern, you - *make each layer as independent as possible*

18. Which of the following is not a benefit of using the MVC pattern for an application? - *it requires less code*

19. Which statement is true for the order in which styles override other styles when you use CSS? - *the first style that’s applied overrides following styles*

20. You can use tables to organize data in - *rows and columns*

21. To minimize the amount of Java code in your JSPs, you - *use servlets to handle the processing requirements*

22. When you use the MVC pattern, the controller directs the flow of control to - *the view and the model*

23. Which directory stores the Java classes of a servlet/JSP application? - *WEB-INF\classes*

24. Which of the following is an API for working with databases? - *JPA*